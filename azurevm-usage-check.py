import os
import os.path
import pytz
from pathlib import Path
import time
import datetime
from azure.common.credentials import ServicePrincipalCredentials
from azure.storage.blob import BlobServiceClient
from azure.storage.blob import BlobClient
from azure.mgmt.compute import ComputeManagementClient
from datetime import datetime
import config as conf

def get_credentials():
    credentials = ServicePrincipalCredentials(
        client_id='0a4c88cc-d722-4c67-9b8f-53005352c85b',
        secret='BQ[Nnd?/7ug3mA.Fqt77e7Dx?D:h_yQ?',
        tenant='f1d4219e-ae49-4782-b878-bd9680019c0a'
    )

    return credentials


def monitoring_time():
    blob_service_client = BlobServiceClient.from_connection_string(conf.STORAGE_ACCOUNT_CONNECTION_STRING)
    container_client = blob_service_client.get_container_client(conf.CONTAINER_NAME)
    blob_list = container_client.list_blobs()
    extension = conf.VM_NAME+conf.RESOURCE_GROUP_NAME
    for blob in blob_list:
        if extension == blob.name[2:len(blob.name)-4]:
            user_requested_time = blob.name[0:2]
            blob_file_time = blob.last_modified
            return blob_file_time, user_requested_time


def check_system_reboot():
    try:
        reboot_log = os.path.exists(os.getcwd()+'\\reboot.txt')
        if reboot_log is True:
            reboot_log_local_time = datetime.strptime(time.ctime(os.path.getmtime(os.getcwd()+'\\reboot.txt')), "%a %b %d %H:%M:%S %Y")
            return reboot_log_local_time
    except:
        print("Reboot LOG Folder Does not exists")


def iis_log_monitored_time():
    try:
        iis_log = [log for log in Path(conf.iis_folder).iterdir()]
        for file in iis_log:
            iis_log_date = time.strftime('%Y-%m-%d', time.gmtime(os.path.getmtime(file)))
            if str(datetime.today()).split()[0] == str(iis_log_date):
                iis_log_time = datetime.strptime(time.ctime(os.path.getmtime(str(file))), "%a %b %d %H:%M:%S %Y")
                return iis_log_time
    except:
        print("IIS LOG Folder Does not exists")


def rdp_session():
    cmd = "quser"
    os.system(cmd + "> temp.txt")
    temp_file = open("temp.txt", "r").readlines()
    rdp_login_status = temp_file[1].split()[3]
    return rdp_login_status


def stop_vm():
    credentials = get_credentials()
    compute_client = ComputeManagementClient(
        credentials,
        conf.SUBSCRIPTION_ID
    )
    compute_client.virtual_machines.deallocate(conf.RESOURCE_GROUP_NAME, conf.VM_NAME)


def schedule_check():
    current_time = datetime.now(pytz.utc)
    if monitoring_time() is not None:
        requested_time_diff = (current_time - monitoring_time()[0]).total_seconds() / 60
        if requested_time_diff < int(monitoring_time()[1]) * 60:
            print("Server Requested To RUN for", monitoring_time()[1], "Hours :: Already Used Time ", requested_time_diff/60, " Hours!!!!!!", '"Script Execution Time" = ', datetime.now())
            return
    if check_system_reboot() is not None:
        reboot_log_modified_diff = (datetime.now() - check_system_reboot()).total_seconds() / 60
        if reboot_log_modified_diff < 120:
            print("Monitoring Skipped due to Server Restarted in last 2 Hours!!!", "Server Reboot Time = ", check_system_reboot(),'    "Script Execution Time" = ', datetime.now())
            return
    if iis_log_monitored_time() is not None:
        iis_log_modified_diff = (datetime.now() - iis_log_monitored_time()).total_seconds() / 60
        if iis_log_modified_diff < 120:
            print("Server is Accessing from Outside!!!!!", '"Script Execution Time" = ', datetime.now())
            return
    if rdp_session() == "Active":
        print("RDP session is Active!!!!!!", '"Script Execution Time" = ', datetime.now())
        return
    else:
        try:
            #stop_vm()
            print("The Requested Instance has been Stopped!!!!!!", '"Script Execution Time" = ', datetime.now())
        except:
            print("Unable to stop VM!!! Stop_VM method exception!!!!!", '"Script Execution Time" = ', datetime.now())
        blob_service_client = BlobServiceClient.from_connection_string(conf.STORAGE_ACCOUNT_CONNECTION_STRING)
        container_client = blob_service_client.get_container_client(conf.CONTAINER_NAME)
        blob_list = container_client.list_blobs()
        extension1 = conf.VM_NAME + conf.RESOURCE_GROUP_NAME
        for blob in blob_list:
            if extension1 == blob.name[2:len(blob.name) - 4]:
                blob_client = BlobClient.from_connection_string(conn_str=conf.STORAGE_ACCOUNT_CONNECTION_STRING, container_name=conf.CONTAINER_NAME, blob_name=blob.name)
                print("Blob File Ready to Delete")
                try:
                    blob_client.delete_blob()
                    print(blob.name, " --> Requested File Deleted")
                except:
                    print("Blob File not found")


schedule_check()


